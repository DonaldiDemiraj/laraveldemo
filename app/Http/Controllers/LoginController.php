<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Login;

class LoginController extends Controller
{
   public function index()
   {
      return view('create_acc');
   }

   public function create(Request $req)
   {
       $name = $req->uname;
       $email = $req->uemail;
       $pass = $req->upass;
       $check_email = Login::where('email',$email)->get();

       if(count($check_email) > 0)
       {
            return redirect('/create_account')->with('msg','Account Email Exists!');
       }
       else{

        $login = new Login;
        $login->name=$name;
        $login->email = $email;
        $login->pass = $pass;

        $created = $login->save();

        if($created){
            return redirect('/login')->with('msg','Account Created Successfull. Please LogIn!');
        }
      }   
    }

   public function login()
   {
       return view('login');
   }

   public function check_user(Request $req)
   {
        $email = $req->uemail;
        $password = $req->upass;

        // $session = new Login;
        // $session->where('email',$email)->where('pass', $password)->get();

        $session =  Login::where('email',$email)->where('pass', $password)->get();

        if(count($session)>0)
        {
            $req->session()->put('user_id', $session[0]->id);
            $req->session()->put('user_name', $session[0]->name);
             
            return redirect('/welcome');
        }
        else{
            return redirect('/login')->with('msg','Email or Password does not mutch');
       }
   }

   public function protect(Request $req)
   {
        if($req->session()->get('user_id') =="")
        {
            return redirect('/login');
        }
        else{
            $username = $req->session()->get('user_name');
            $capsule = array('username' => $username);
            
            return view('protect')->with($capsule);
        }
   }

   public function logout(Request $req)
   {
       $req->session()->forget('user_id');
       $req->session()->forget('user_name');

       return redirect('/login'); 
   }
}
