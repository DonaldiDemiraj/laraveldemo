@extends('master')

@section('content')
    <br>
    <div class="container">
        <div class="card">
            <div class="card-body">
                @if (session()->get('success'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ session()->get('success')}}

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                <form action="create" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-4">
                            <label>Name</label>

                            <input type="text" name="uname" class="form-control" required>
                        </div>

                        <div class="col-md-4">
                            <label>Email</label>

                            <input type="text" name="uemail" class="form-control" required>
                        </div>

                        <div class="col-md-4">
                            <label>Password</label>

                            <input type="password" name="upass" class="form-control" required>
                        </div>
                        
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary">Create Account</button>
                </form>
            </div>
        </div>
    </div>
@endsection