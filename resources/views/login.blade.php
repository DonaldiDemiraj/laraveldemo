@extends('master')

@section('content')
<br>
<div class="container">
    <div class="card">
        <div class="card-body">
            @if (session()->get('msg'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session()->get('msg')}}

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            <form action="check" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <label>Enter Email</label>

                        <input type="text" name="uemail" class="form-control" required>
                    </div>

                    <div class="col-md-6">
                        <label>Enter Password</label>

                        <input type="password" name="upass" class="form-control" required>
                    </div>
                    
                </div>
                <br>
                <button type="submit" class="btn btn-primary">Login</button>
            </form>
        </div>
    </div>
</div>
@endsection