@extends('master')

@section('content')
<br>
<div class="container">
    <div class="card">
        <div class="card-body">
            <h1>Welcome {{ $username }}</h1>
            <p>
                <a href="{{ url('/logout')}}" class="btn btn-danger">Logout</a>
            </p>
        </div>
    </div>
</div>
@endsection