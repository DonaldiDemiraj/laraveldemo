<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/create_account', [LoginController::class, 'index']);

Route::post('/create', [LoginController::class, 'create']);

Route::get('/login', [LoginController::class, 'login']);

Route::post('/check', [LoginController::class, 'check_user']);

Route::get('/welcome', [LoginController::class, 'protect']);

Route::get('/logout', [LoginController::class, 'logout']);